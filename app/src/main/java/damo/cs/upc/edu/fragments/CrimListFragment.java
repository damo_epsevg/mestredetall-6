package damo.cs.upc.edu.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by josepm on 30/6/16.
 */
public class CrimListFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_crim_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);  // On es visualitza la llista de crims
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        MagatzemCrims crims = MagatzemCrims.obtenirMagatzem();      // Model
        CrimAdapter adapter = new CrimAdapter(crims);               // Adapter

        recyclerView.setAdapter(adapter);

        return v;
    }

    class CrimAdapter extends RecyclerView.Adapter<CrimHolder> {

        MagatzemCrims crims;

        public CrimAdapter(MagatzemCrims crims) {
            this.crims = crims;

        }


        @Override
        public CrimHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.fila_crim, parent, false);

            return new CrimHolder(view);
        }


        @Override
        public void onBindViewHolder(CrimHolder holder, int position) {
            Crim crim = crims.getCrim(position);

            holder.bindCrim(crim);

        }


        @Override
        public int getItemCount() {
            return crims.getCount();
        }


    }


    class CrimHolder extends RecyclerView.ViewHolder {
        private TextView titol_fila;
        private TextView data_fila;
        private CheckBox solucionat_fila;


        public CrimHolder(View view) {
            super(view);
            titol_fila = (TextView) view.findViewById(R.id.titol_crim_fila);
            data_fila = (TextView) view.findViewById(R.id.data_crim_fila);
            solucionat_fila = (CheckBox) view.findViewById(R.id.solucionat_fila);
        }

        public void bindCrim(Crim crim) {
            titol_fila.setText(crim.getTitol());
            data_fila.setText(crim.getData().toString());
            solucionat_fila.setChecked(crim.getSolucionat());
        }
    }

}
