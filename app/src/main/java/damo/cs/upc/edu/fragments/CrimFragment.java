package damo.cs.upc.edu.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

/**
 * Created by Josep M on 17/06/2016.
 */
public class CrimFragment extends Fragment {

    private static String ARG = "Argument";

    private String argument;

    private EditText entradaTitol;
    private CheckBox checkSolucionat;

    private Crim crim = new Crim();

    static CrimFragment getInstance(String argument) {
        CrimFragment f = new CrimFragment();
        Bundle b = new Bundle();
        b.putString(ARG, argument);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        argument = getArguments().getString(ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.crim_fragment, container, false);
        programaWidgets(v);

        return v;

    }

    private void programaWidgets(View v) {
        entradaTitol = (EditText) v.findViewById(R.id.titol_crim);
        checkSolucionat = (CheckBox) v.findViewById(R.id.crime_solved);

        entradaTitol.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                crim.setTitol(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        checkSolucionat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                crim.setSolucionat(isChecked);
            }
        });
    }


}
