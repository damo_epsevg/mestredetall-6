package damo.cs.upc.edu.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by josepm on 30/6/16.
 */
public class CrimListActivity extends SingleFragmentActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }

    @Override
    protected Fragment getInstance() {
        return new CrimListFragment();
    }
}
