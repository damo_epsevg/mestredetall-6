/**
 * HOLA ANDROID
 */


package damo.cs.upc.edu.fragments;


import android.support.v4.app.Fragment;

public class CrimActivity extends SingleFragmentActivity {

    @Override
    public Fragment getInstance() {
        return CrimFragment.getInstance("");
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }
}
